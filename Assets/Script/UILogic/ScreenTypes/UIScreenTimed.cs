﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIScreenTimed : UIScreen
{
    [Header("Time Screen Properties")]
    [SerializeField] private float _screenWaitTimeSecound = 2f;
    [SerializeField] private UnityEvent _onTimeCompleted = new UnityEvent();

    private float _startTime;

    public override void StartScreen()
    {
        base.StartScreen();

        _startTime = Time.time;

        StartCoroutine(WaitForTime());
    }

    private IEnumerator WaitForTime()
    {
        yield return new WaitForSeconds(_screenWaitTimeSecound);

        _onTimeCompleted?.Invoke();
    }
}
