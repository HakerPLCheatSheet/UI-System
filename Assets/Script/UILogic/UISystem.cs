﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UISystem : MonoBehaviour
{
    [Header("Main Properties")]
    [SerializeField] private UIScreen _startScreen;
    [SerializeField] private Component[] _screens = new Component[0];


    [Header("System Events")]
    [SerializeField] private UnityEvent _onSwitchedScreen = new UnityEvent();

    [Header("Fader Properties")]
    [SerializeField] private Image _fader;
    [SerializeField] private float _fadeInDuration = 1f;
    [SerializeField] private float _fadeOutDuration = 1f;

    private UIScreen _currentScreen;
    public UIScreen CurrentScreen => _currentScreen;

    private UIScreen _previousScreen;
    public UIScreen PreviousScreen => _previousScreen;

    // Start is called before the first frame update
    private void Start()
    {
        _screens = GetComponentsInChildren<UIScreen>(true);
        InitializeScreens();

        if (_startScreen)
        {
            SwitchScreen(_startScreen);
        }

        if (_fader)
        {
            _fader.gameObject.SetActive(true);
        }

        FadeIn();
    }

    public void SwitchScreen(UIScreen screen)
    {
        if (screen)
        {
            FadeOut();

            if (_currentScreen)
            {
                _currentScreen.Close();
                _previousScreen = _currentScreen;
            }

            _currentScreen = screen;

            _currentScreen.gameObject.SetActive(true);
            _currentScreen.StartScreen();

            _onSwitchedScreen?.Invoke();

            FadeIn();
        }
    }

    public void FadeIn()
    {
        if (_fader)
        {
            _fader.CrossFadeAlpha(0f, _fadeInDuration, false);
        }
    }

    public void FadeOut()
    {
        if (_fader)
        {
            _fader.CrossFadeAlpha(1f, _fadeOutDuration, false);
        }
    }

    public void GoToPreviousScreen()
    {
        if (_previousScreen)
        {
            SwitchScreen(_previousScreen);
        }
    }

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(WaitToLoadScene(sceneIndex));
    }

    private IEnumerator WaitToLoadScene(int sceneIndex)
    {
        yield return null;
    }

    private void InitializeScreens()
    {
        foreach (var screen in _screens)
        {
            screen.gameObject.SetActive(true);
        }
    }
}
