﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CanvasGroup))]
public class UIScreen : MonoBehaviour
{
    [SerializeField] private Selectable _startSelectable;

    [Header("Screen Events")]
    [SerializeField] private UnityEvent _onScreenStart = new UnityEvent();
    [SerializeField] private UnityEvent _onScreenClose = new UnityEvent();

    private Animator _animator;

    // Start is called before the first frame update
    private void Start()
    {
        _animator = GetComponent<Animator>();

        if (_startSelectable)
        {
            //set selected color, sprite or animation to highlighted 
            EventSystem.current.SetSelectedGameObject(_startSelectable.gameObject);
        }
    }

    public virtual void StartScreen()
    {
        TriggerAnimation("Show");
        _onScreenStart?.Invoke();
    }

    public virtual void Close()
    {
        TriggerAnimation("Hide");
        _onScreenClose?.Invoke();
    }

    private void TriggerAnimation(string trigger)
    {
        if (_animator)
        {
            _animator.SetTrigger(trigger);
        }
    }
}
